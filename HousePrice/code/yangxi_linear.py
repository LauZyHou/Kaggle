import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

'''
参考杨熹:我是如何从 0 到 1 走进 Kaggle 的
https://wemedia.ifeng.com/16578677/wemedia.shtml
'''

train = pd.read_csv("../data/train.csv")
test = pd.read_csv("../data/test.csv")
# print(train.shape, test.shape, sep='\n')

# 看看对于标签值的相关性:最相关和最不相关的5个
numeric_features = train.select_dtypes(include=np.number)
# print(numeric_features.dtypes)
corr = numeric_features.corr()  # 相关性矩阵
# print(corr['SalePrice'].sort_values(ascending=False)[:5], '\n')
# print(corr['SalePrice'].sort_values(ascending=False)[-5:])

# 处理极端值
train = train[train['GarageArea'] < 1200]
print(train.shape, type(train))

# 处理缺失值:对于数值形式的数据,先用默认interpolate()进行插值,再删除那些有NaN的行
data = train.select_dtypes(include=[np.number]).interpolate().dropna()
print(data.shape, type(data))  # 从这里的行数没变就可以看出没有删掉任何行

# 对Street列添加one-hot编码:见https://blog.csdn.net/dongyanwen6036/article/details/78555163
# 这里要用train.Street因为data里只包含了有数值型数据的列,已经没有Street这一列了
data['enc_street'] = pd.get_dummies(train.Street, drop_first=True)
test['enc_street'] = pd.get_dummies(test.Street, drop_first=True)

# 取出标签值y和特征矩阵X,对标签值还处理了正偏度
y = np.log(train.SalePrice)
X = data.drop(["SalePrice", "Id"], axis=1)
# 划分训练集和验证集
X_train, X_cv, y_train, y_cv = train_test_split(X, y, random_state=0, test_size=.33)

# 线性回归模型
lr = linear_model.LinearRegression()
model = lr.fit(X_train, y_train)
predictions = model.predict(X_cv)
print("MSE Loss:", mean_squared_error(y_cv, predictions))

# 对测试文件进行预测,先进行预处理,删除对分类无关的Id列,然后进行插值,这里不能删除NaN行
feats = test.select_dtypes(include=np.number).drop(["Id"], axis=1).interpolate()
predictions = model.predict(feats)
# 注意因为模型训练时对标签取了对数,这里预测完要变回来
predictions = np.exp(predictions)

# 生成结果csv
submissions = pd.DataFrame()
submissions["Id"] = test.Id
submissions["SalePrice"] = predictions
submissions.to_csv("upload.csv", index=False)
