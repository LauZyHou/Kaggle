import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.stats import norm
from scipy import stats

'''
参考杨熹:我是如何从 0 到 1 走进 Kaggle 的
https://wemedia.ifeng.com/16578677/wemedia.shtml
'''

df = pd.read_csv("../data/train.csv")
# print(df['SalePrice'].describe())

# 绘制相关性矩阵
corrmat = df.corr()
fig, ax = plt.subplots(figsize=(12, 9))
sns.heatmap(corrmat, vmax=.8, square=True, cmap='coolwarm')
plt.show()

# 绘制和SalePrice最紧密关联的10个变量的关联度
k = 10  # heatmap的变量数目
cols = corrmat.nlargest(k, 'SalePrice')['SalePrice'].index
cm = np.corrcoef(df[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values,
                 xticklabels=cols.values)
plt.show()

# 查看缺失值
total = df.isnull().sum().sort_values(ascending=False)
percent = (df.isnull().sum() / df.isnull().count()).sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
# print(missing_data.head(20))

# 处理缺失值
df = df.drop((missing_data[missing_data['Total'] > 1]).index, 1)
df = df.drop(df.loc[df['Electrical'].isnull()].index)

# 查看极端值
# 双变量分析:saleprice/grlivarea
var = 'GrLivArea'
data = pd.concat([df['SalePrice'], df[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0, 800000))
plt.show()

# 删除图上看到的极端值
# print(df.sort_values(by='GrLivArea', ascending=False)[:2])
# 从上面的输出看到这两个极端值的Id=1299和524,或者直接用index=1298和523也可以
df = df.drop(df[df['Id'] == 1299].index)
df = df.drop(df[df['Id'] == 524].index)
data = pd.concat([df['SalePrice'], df[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0, 800000))
plt.show()

# 绘制Probability Plot
res = stats.probplot(df['SalePrice'], plot=plt)
plt.show()

# 看看SalePrice的频度直方图,这里拟合标准正态分布
sns.distplot(df['SalePrice'], fit=norm)  # 这里需要修改seaborn的源文件
plt.show()  # 这里看到SalePrice是正偏度

# 在正偏度下取对数转换成正态分布,然后再看一下转换后的
df['SalePrice'] = np.log(df['SalePrice'])
sns.distplot(df['SalePrice'], fit=norm)
plt.show()

# 进行对数转换前后的GrLivArea的形状变化
_, axes = plt.subplots(1, 2, figsize=(12, 6))
axes[0].scatter(df['GrLivArea'], df['SalePrice'])
df['GrLivArea'] = np.log(df['GrLivArea'])
axes[1].scatter(df['GrLivArea'], df['SalePrice'])
plt.show()
